### SIGLAS

GNU : new
GNU is Not Unix
GNU is Not  Unix is Not Unix

BNF: Notación de Backus Naur
     [] : 	Opcional
     <nombre> : Algo de este tipo
     | :	Or

### Extensiones

.md => Markdown (HTML rápido)

### Conceptos

SO + Distro + Flavour: (GNU + Debian + Ubuntu)
Carpeta: No existe
Directorio: Lo que la gente llama carpeta.
Fichero: Datos guardados en el disco duro
Archivo: Fichero que contiene múltiples ficheros (zip, rar,etc.).

ASCII: Codificación de caracteres. American Standard Code for Information Interchange
Unicode: Codificación de caracteres que amplia ASCII.
UTF: Formato de Texto Unicode

# Teclado

## Hardware

CHIP de Teclado : Genera SCAN CODES (Make, Release)
Buffer circular (Pita cuando lleno)
Puerto de teclado: 0x60

## ATAJOS DE TECLADO (shortcuts)

### Escritorio

C-Alt-t: Sacar la terminal.
M-↑: Maximizar ventana

### Generales

C-s: Guardar
C-q: Terminar programa
C-w: Cerrar ventana

### Explorador de Archivos

C-l: Barra de direcciones
C-+: Apmlía los iconos.
C--: Reduce los iconos.

### Terminal
C-l: Limpia la pantalla.

## SISTEMA DE FICHEROS (FHS - File Hierarchy System)

/: SO (GNU/Linux)
/usr: Distro (Debian)
/usr/local: Administrador del Sistema
/home/<yo>: Instalaciones de un usuario.

/: Raíz - De donde cuelga todo

/bin: Ficheros ejecutables
/sbin: Ejecutables del admnistrador del sistema.

Carpeta Personal: (Documentos, Descargas, Escritorio, etc.) /home/txema

Rutas absolutas
Rutas relativas
Directorios no existen => Son sólo el nombre

### Prompt
<user>@<machine>: <dir><$ | #>

## Manejos BASH

Comentarios: #
Shebang: #! => Con cual programa hay que interpretar esto.
$1: El primer parámetro

### Variables de Entorno
PATH: Lista de directorios con ejecutables
HOME: Directorio casa para el usuario

### Metacaracter
$: Dime el valor de
.: El directorio actual
..: El directorio de arriba

### Comando
man, info: Ayuda de programas
	1: User commands
	2: Llamadas al sistema
	3: Programación
	4: Ficheros especiales (/dev)
	5: Formato de Ficheros
	6: Juegos
	7: Macros y Convenios
	8: Comandos de administración
	9: Rutinas del núcleo
	

firefox (howto, vs)
help: Ayuda de comandos built-in

echo: Muestra en pantalla sin ejecutar
chmod: Cambia los permisos de un fichero.
	+x Da permiso de ejecución
        +w Da permiso de escritura
        +r Da permiso de lectura
        u,g,o [usuario, grupo, otros]
chown: Cambia el propietario
mkdir: Crea un directorio	
mv: Cambiar nombres = mover a otro directorio
cd: Cambia al directorio
pwd: Present Working Directory

cat: Muestra el contenido de un fichero

## Gestión de Usuarios
who: Usuarios conectados
whoami: Who am I?

cat /etc/passwd:	Ver usuarios
ls /home:		Ver usuarios
passwd -Sa:		Ver usuarios

useradd: Crea usuarios
	-m:	Crea el dir home
	-g:	Grupo inicial
	-G:	Grupos adicionales
	-s:	Shell

adduser: No son iguales
passwd:		Cambia la contraseña del usuario
newgrp:		Crea grupos
usermod:	Modifica el usario
		-d Cambia el directorio home
		-l <nuevo login> <usuario actual>
userdel:	Borra usarios.
users:		Usuarios loggeados

groups:		Grupos a los que pertenece el usuario
groupadd:	Añadir grupos
groupdel:	Borrar grupo
groupmod:	Modifica los grupos

gpasswd:	Administramos los grupos
chage:		Decimos cuando expira la contraseña

su:	Hacemos como que somos otro usuario

### Ficheros
/etc/passwd: Todos los usuarios
login:passwd:uid:gid:comments:home:exe_inicio
comments => Nombre,tel, tel casa, email, edificio, habitacion, nº
/etc/shadow => Información segura del usuario.
/etc/gshadow => Información segura de los grupos.
/etc/group => Grupos del sistema
/etc/sudoers => Políticas de seguridad. (Quién y cómo hace sudo)




