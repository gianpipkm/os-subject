## Competencias

1. Sé instalar un sistema operativo
1. Sé instalar un sistema de ventanas.
1. Conozco perfectamente la jerarquía del sistema de ficheros (FHS)
1. Sé hacer instalaciones per single user.

1. Dar de alta y gestionar usuarios
1. Crear grupos y añadir usarios al grupo
1. Soy capaz de darme de baja de un grupo

1. Copiar archivos en la misma máquina
1. Copiar archivos entre distintas máquinas
1. Hacer redirecciones de entrada / salida / tuberías

1. Configurar la red (modo visual / modo texto)

1. Hacer un chroot básico
1. chroot en contenedores deboostrap
1. chroot en contenedores docker

1. Montar y desmontar una unidad real / virtual
1. Saber mandar archivos por netcat troceados con split.
1. Saber mandar archivos con netcat comprimidos en paralelo con pigz
1. Saber comprimir y descomprimir con gzip y gunzip
1. Ver los servicios en ejecución netstat
1. Analizar una red con nmap

1. Hablar 311173
1. Soy capaz de resolver un problema cuando no tengo el conocimiento.
