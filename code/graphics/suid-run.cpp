#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    setuid( 0  );
    if (argc < 2){
        fprintf( stderr,
                "%s - runs a shell script with SUID.\n"
                "\t%s <script>\n"
                "Not enough arguments.\n", argv[0], argv[0] );
    }

    system( argv[1]  );

    return 0;
}

